import { inject, Injectable } from '@angular/core';
import { CanActivateFn, Router } from '@angular/router';

import { AuthenticationService } from '@mandiri/service';

import { filter, map, take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
class AuthService {
  constructor(private router: Router, private authenticationService: AuthenticationService) {}

  canActivate() {
    return this.authenticationService.getCurrentUser().pipe(
      filter((val) => val !== null),
      take(1),
      map((isAuthenticated) => !!isAuthenticated)
    );
  }
}

export const AuthGuard: CanActivateFn = () => {
  return inject(AuthService).canActivate();
};
