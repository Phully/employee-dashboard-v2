import { Component } from '@angular/core';

import { NzButtonComponent } from 'ng-zorro-antd/button';
import { NzContentComponent, NzHeaderComponent, NzLayoutComponent } from 'ng-zorro-antd/layout';
import { Router, RouterOutlet } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { AuthenticationService } from '@mandiri/service';

@Component({
  standalone: true,
  selector: 'lib-layout-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.less'],
  imports: [NzLayoutComponent, NzButtonComponent, NzContentComponent, NzHeaderComponent, RouterOutlet]
})
export class DashboardComponent {
  constructor(private router: Router, private message: NzMessageService, private authenticationService: AuthenticationService) {}

  signOutStatus = false;

  async onSignOutClick() {
    this.signOutStatus = true;

    const { error } = await this.authenticationService.signOut();

    this.signOutStatus = false;

    if (error) {
      this.message.error(error.message, { nzDuration: 1500 });
      throw new Error(error.message);
    }

    this.message.success('SignOut Successful!', { nzDuration: 3000 });
    await this.router.navigateByUrl(`/`);
  }
}
