export type EmployeeOptions = {
  supabaseUrl: string;
  supabaseKey: string;
};

export type EmployeeType = {
  id: number;
  username: string;
  firstName: string;
  lastName: string;
  email: string;
  birthDate: string;
  basicSalary: number;
  description: string;
  group: string;
  status: number;
};

export type FindAllEmployeeRequest = {
  search?: string;
  page: number;
  pageSize: number;
};

export type FindAllEmployeeResponse = {
  data: EmployeeType[];
  totalData: number;
};

export type FindOneEmployeeRequest = {
  id: number;
};

export type FindOneEmployeeResponse = {
  data: EmployeeType;
  totalData: number;
};

export type UpsertEmployee = Omit<EmployeeType, 'id'>;
