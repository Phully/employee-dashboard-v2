import { Inject, Injectable } from '@angular/core';
import { SupabaseClient, createClient } from '@supabase/supabase-js';

import { BehaviorSubject, filter, take } from 'rxjs';
import { SnakeCasedPropertiesDeep } from 'type-fest';

import { EMPLOYEE_OPTIONS } from './employee.const';
import { EmployeeOptions, EmployeeType, FindAllEmployeeRequest, FindAllEmployeeResponse, FindOneEmployeeRequest, FindOneEmployeeResponse, UpsertEmployee } from './employee.types';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  private supabase: SupabaseClient;

  employees: BehaviorSubject<FindAllEmployeeResponse | null> = new BehaviorSubject<FindAllEmployeeResponse | null>(null);

  employee: BehaviorSubject<FindOneEmployeeResponse | null> = new BehaviorSubject<FindOneEmployeeResponse | null>(null);

  constructor(@Inject(EMPLOYEE_OPTIONS) authenticationOptions: EmployeeOptions) {
    this.supabase = createClient(authenticationOptions.supabaseUrl, authenticationOptions.supabaseKey);
  }

  async findAll(dto?: FindAllEmployeeRequest) {
    const search = dto?.search || '';
    const page = dto?.page || 1;
    const pageSize = dto?.pageSize || 10;

    const { data, error, count } = await this.supabase
      .from('employee')
      .select('*', { count: 'exact' })
      .or(`username.ilike.%${search}%, first_name.ilike.%${search}%, last_name.ilike.%${search}%, email.ilike.%${search}%`)
      .range((page - 1) * pageSize, page * pageSize - 1);

    if (error) {
      throw error.message;
    } else {
      const employeeData: FindAllEmployeeResponse = { data: this.#transformEmployeeData(data), totalData: Number(count) };
      this.employees.next(employeeData);
      return employeeData;
    }
  }

  async findOne(dto: FindOneEmployeeRequest) {
    const { id } = dto;

    let employee: EmployeeType | null = null;

    this.employees
      .pipe(take(1))
      .pipe(filter((employees) => !!employees?.data?.find((employee) => employee.id === id)))
      .subscribe((employees) => (employee = employees?.data?.[0] || null));

    if (employee) {
      return employee;
    }

    const { data: employeeData, error: employeeError } = await this.supabase.from('employee').select('*').eq('id', id).limit(1);

    if (employeeError) {
      throw employeeError.message;
    } else {
      const transformEmployeeData = this.#transformEmployeeData(employeeData);
      const responseData: FindOneEmployeeResponse = { data: transformEmployeeData?.[0] || null, totalData: transformEmployeeData.length };
      this.employee.next(responseData);
      return responseData;
    }
  }

  async upsert(dto: UpsertEmployee) {
    return this.supabase.from('employee').upsert({
      username: dto.username,
      first_name: dto.firstName,
      last_name: dto.lastName,
      email: dto.email,
      birth_date: dto.birthDate,
      basic_salary: dto.basicSalary,
      group: dto.group,
      description: dto.description,
      status: dto.status
    });
  }

  #transformEmployeeData(employees: SnakeCasedPropertiesDeep<EmployeeType[]>) {
    return employees.map((employee) => ({
      id: employee.id,
      username: employee.username,
      firstName: employee.first_name,
      lastName: employee.last_name,
      email: employee.email,
      birthDate: employee.birth_date,
      basicSalary: employee.basic_salary,
      group: employee.group,
      description: employee.description,
      status: employee.status
    }));
  }
}
