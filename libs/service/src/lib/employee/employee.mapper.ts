export const EMPLOYEE_GROUP_MAPPER = [
  'Risk Management',
  'Operations',
  'Quality Assurance',
  'Sales',
  'Technology',
  'Marketing',
  'Creative Services',
  'Production',
  'Product Management',
  'Human Resources'
];
