export type AuthenticationOptions = {
  supabaseUrl: string;
  supabaseKey: string;
};

export type SigninType = {
  email: string;
  password: string;
};
