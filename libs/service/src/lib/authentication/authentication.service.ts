import { Inject, Injectable } from '@angular/core';

import { SupabaseClient, User, createClient } from '@supabase/supabase-js';
import { BehaviorSubject } from 'rxjs';

import { AUTHENTICATION_OPTIONS } from './authentication.const';
import { AuthenticationOptions, SigninType } from './authentication.types';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  private supabase: SupabaseClient;

  private user: BehaviorSubject<User | null> = new BehaviorSubject<User | null>(null);

  constructor(@Inject(AUTHENTICATION_OPTIONS) authenticationOptions: AuthenticationOptions) {
    this.supabase = createClient(authenticationOptions.supabaseUrl, authenticationOptions.supabaseKey);

    this.supabase.auth.onAuthStateChange((event, session) => {
      if (session && (event === 'SIGNED_IN' || event === 'TOKEN_REFRESHED')) this.user.next(session.user);
      else this.user.next(null);
    });
  }

  async loadUser() {
    if (!this.user.value) {
      const user = await this.supabase.auth.getUser();
      if (user.data.user) this.user.next(user.data.user);
      else this.user.next(null);
    }
  }

  signIn(dto: SigninType) {
    return this.supabase.auth.signInWithPassword(dto);
  }

  signOut() {
    return this.supabase.auth.signOut();
  }

  getCurrentUser() {
    return this.user;
  }
}
