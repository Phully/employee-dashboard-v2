import { Component, OnInit } from '@angular/core';
import { ReactiveFormsModule, UntypedFormBuilder, Validators, UntypedFormGroup } from '@angular/forms';
import { Router } from '@angular/router';

import { SigninType, AuthenticationService } from '@mandiri/service';

import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzMessageService } from 'ng-zorro-antd/message';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.less'],
  imports: [ReactiveFormsModule, NzFormModule, NzButtonModule, NzInputModule, NzCheckboxModule, NzGridModule],
  providers: [NzMessageService],
  standalone: true
})
export class SigninComponent implements OnInit {
  constructor(private router: Router, private formBuilder: UntypedFormBuilder, private message: NzMessageService, private authenticationService: AuthenticationService) {}

  form!: UntypedFormGroup;

  submitStatus!: boolean;

  autoTips!: Record<string, Record<string, string>>;

  ngOnInit() {
    this.form = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(20)]],
      remember: [false, []]
    });

    this.autoTips = {
      default: { required: 'The Input is required', email: 'The input is not valid email' }
    };

    this.submitStatus = false;
  }

  async onSigninSubmit() {
    const formValue = this.form.value as SigninType;

    if (!this.form.valid) {
      throw new Error('Form is invalid');
    }

    const { error, data } = await this.authenticationService.signIn(formValue);

    if (error) {
      this.message.error(error.message || 'Please check your username and password!', { nzDuration: 1500 });
      throw new Error(error.message || 'Please check your username and password!');
    }

    this.message.success(`Welcome, ${data.user.email}!`, { nzDuration: 3000 });
    await this.router.navigateByUrl(`/employee`);
  }
}
