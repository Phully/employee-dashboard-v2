import { Component, OnInit } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AuthenticationService } from '@mandiri/service';

import { from } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './main.component.html',
  styleUrl: './main.component.less',
  imports: [RouterModule],
  standalone: true
})
export class MainComponent implements OnInit {
  constructor(private authenticationService: AuthenticationService) {}

  ngOnInit() {
    from(this.authenticationService.loadUser());
  }
}
