import { AUTHENTICATION_OPTIONS, AuthenticationOptions, AuthenticationService, EMPLOYEE_OPTIONS, EmployeeOptions, EmployeeService } from '@mandiri/service';
import { SUPABASE } from '@mandiri/environment';

export const provideService = () => {
  return [
    AuthenticationService,
    EmployeeService,
    { provide: AUTHENTICATION_OPTIONS, useValue: { supabaseUrl: SUPABASE.URL, supabaseKey: SUPABASE.KEY } as AuthenticationOptions },
    { provide: EMPLOYEE_OPTIONS, useValue: { supabaseUrl: SUPABASE.URL, supabaseKey: SUPABASE.KEY } as EmployeeOptions }
  ];
};
