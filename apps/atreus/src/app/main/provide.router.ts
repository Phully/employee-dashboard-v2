import { provideRouter as provideRouterAngular } from '@angular/router';

import { AuthGuard } from '@mandiri/core';
import { DashboardComponent } from '@mandiri/ui';

import { SigninComponent } from '../authentication/signin/signin.component';
import { EmployeeListComponent } from '../employee/employee-list/employee-list.component';
import { EmployeeUpsertComponent } from '../employee/employee-upsert/employee-upsert.component';

export const provideRouter = () => {
  return provideRouterAngular([
    {
      path: '',
      pathMatch: 'full',
      redirectTo: 'signin'
    },
    {
      path: 'signin',
      component: SigninComponent
    },
    {
      path: 'employee',
      component: DashboardComponent,
      canActivate: [AuthGuard],
      children: [
        {
          path: '',
          component: EmployeeListComponent
        },
        {
          path: 'upsert',
          component: EmployeeUpsertComponent
        }
      ]
    }
  ]);
};
