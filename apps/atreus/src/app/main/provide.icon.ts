import { NZ_ICONS } from 'ng-zorro-antd/icon';

import { IconDefinition } from '@ant-design/icons-angular';

import * as icons from '@ant-design/icons-angular/icons';

const antDesignIcons = icons as { [key: string]: IconDefinition };
const antDesignIconsDefinition: IconDefinition[] = Object.keys(antDesignIcons).map((key) => antDesignIcons[key]);

export const provideIcons = () => {
  return { provide: NZ_ICONS, useValue: antDesignIconsDefinition };
};
