import { en_US, NZ_I18N } from 'ng-zorro-antd/i18n';

export const provideLanguage = () => {
  return { provide: NZ_I18N, useValue: en_US };
};
