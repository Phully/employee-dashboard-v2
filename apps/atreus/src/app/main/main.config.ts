import { ApplicationConfig } from '@angular/core';
import { provideHttpClient } from '@angular/common/http';
import { provideAnimations } from '@angular/platform-browser/animations';

import { provideIcons } from './provide.icon';
import { provideLanguage } from './provide.language';
import { provideService } from './provide.service';
import { provideRouter } from './provide.router';

export const mainConfig: ApplicationConfig = {
  providers: [provideHttpClient(), provideAnimations(), provideRouter(), provideService(), provideIcons(), provideLanguage()]
};
