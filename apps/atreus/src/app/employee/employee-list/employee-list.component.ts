import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { DatePipe, KeyValuePipe, NgForOf, NgIf } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { EmployeeService, EmployeeType } from '@mandiri/service';

import { BehaviorSubject, debounceTime, distinctUntilChanged, from, fromEvent, map } from 'rxjs';

import { NzTableModule, NzTableQueryParams } from 'ng-zorro-antd/table';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzTypographyModule } from 'ng-zorro-antd/typography';
import { NzBadgeModule } from 'ng-zorro-antd/badge';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzDrawerModule, NzDrawerRef } from 'ng-zorro-antd/drawer';
import { NzDescriptionsModule } from 'ng-zorro-antd/descriptions';

import { EmployeeQueryParamsType } from './employee-list.type';

@Component({
  standalone: true,
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.less'],
  imports: [
    FormsModule,
    RouterModule,
    NgForOf,
    NgIf,
    DatePipe,
    KeyValuePipe,
    NzTableModule,
    NzGridModule,
    NzButtonModule,
    NzInputModule,
    NzTypographyModule,
    NzBadgeModule,
    NzDividerModule,
    NzIconModule,
    NzDrawerModule,
    NzDescriptionsModule
  ],
  providers: [NzMessageService]
})
export class EmployeeListComponent implements OnInit, AfterViewInit {
  constructor(private employeeService: EmployeeService) {}

  @ViewChild('employeeSearch') employeeSearch!: ElementRef;

  @ViewChild('employeeDrawer') employeeDrawer!: NzDrawerRef;

  employee!: EmployeeType;

  employees: EmployeeType[] = [];

  employeeTotalTable = 0;

  employeeQueryParams: BehaviorSubject<EmployeeQueryParamsType | null> = new BehaviorSubject<EmployeeQueryParamsType | null>(null);

  ngOnInit() {
    this.onSubscribeData();
  }

  ngAfterViewInit() {
    fromEvent(this.employeeSearch.nativeElement, 'keyup')
      .pipe(
        map((event) => (<HTMLInputElement>(<KeyboardEvent>event).target).value),
        debounceTime(500),
        distinctUntilChanged()
      )
      .subscribe((search: string) => {
        if (this.employeeQueryParams.value !== null) this.employeeQueryParams.next({ ...this.employeeQueryParams.value, pageIndex: 1, search: search });
      });
  }

  onSubscribeData() {
    this.employeeService.employees.subscribe((employees) => {
      if (employees) {
        this.employees = employees.data;
        this.employeeTotalTable = employees.totalData;
      }
    });

    this.employeeQueryParams.subscribe((queryParams) => {
      if (queryParams) from(this.employeeService.findAll({ search: queryParams.search, page: queryParams.pageIndex, pageSize: queryParams.pageSize }));
    });
  }

  onEmployeeInformationClose() {
    this.employeeDrawer.close();
  }

  onEmployeeInformationOpen(employee: EmployeeType) {
    this.employee = employee;
    this.employeeDrawer.open();
  }

  onEmployeeTableChange(tableQueryPrams: NzTableQueryParams) {
    this.employeeQueryParams.next({ ...tableQueryPrams, search: this.employeeQueryParams?.value?.search || '' });
  }
}
