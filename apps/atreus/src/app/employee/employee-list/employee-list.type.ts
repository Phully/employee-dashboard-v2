import { NzTableQueryParams } from 'ng-zorro-antd/table';

export type EmployeeQueryParamsType = NzTableQueryParams & { search: string };
