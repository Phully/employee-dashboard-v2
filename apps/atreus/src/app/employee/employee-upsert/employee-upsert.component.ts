import { Component, OnInit } from '@angular/core';
import { NgForOf, NgIf } from '@angular/common';
import { Router, RouterModule } from '@angular/router';
import { ReactiveFormsModule, UntypedFormBuilder, Validators, UntypedFormGroup } from '@angular/forms';

import { EmployeeService, EMPLOYEE_GROUP_MAPPER } from '@mandiri/service';

import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzTypographyModule } from 'ng-zorro-antd/typography';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzDatePickerModule } from 'ng-zorro-antd/date-picker';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzInputNumberModule } from 'ng-zorro-antd/input-number';

import { DataFormType } from './employee-upsert.type';

@Component({
  standalone: true,
  selector: 'app-employee-upsert',
  templateUrl: './employee-upsert.component.html',
  styleUrls: ['./employee-upsert.component.less'],
  imports: [
    RouterModule,
    ReactiveFormsModule,
    NgForOf,
    NgIf,
    NzFormModule,
    NzInputModule,
    NzDatePickerModule,
    NzSelectModule,
    NzInputNumberModule,
    NzTypographyModule,
    NzButtonModule
  ],
  providers: [NzMessageService]
})
export class EmployeeUpsertComponent implements OnInit {
  constructor(private router: Router, private formBuilder: UntypedFormBuilder, private message: NzMessageService, private employeeService: EmployeeService) {}

  form!: UntypedFormGroup;

  formData!: DataFormType[];

  autoTips!: Record<string, Record<string, string>>;

  ngOnInit() {
    this.form = this.formBuilder.group({
      username: ['', [Validators.required]],
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      email: ['', [Validators.email, Validators.required]],
      birthDate: ['', [Validators.required]],
      basicSalary: ['', [Validators.required]],
      group: ['', [Validators.required]],
      description: ['', [Validators.required]],
      status: ['', [Validators.required]]
    });

    this.formData = [
      {
        name: 'username',
        title: 'Username',
        type: 'text',
        formatter: () => 0,
        parser: () => ''
      },
      {
        name: 'firstName',
        title: 'First Name',
        type: 'text',
        formatter: () => 0,
        parser: () => ''
      },
      {
        name: 'lastName',
        title: 'Last Name',
        type: 'text',
        formatter: () => 0,
        parser: () => ''
      },
      {
        name: 'email',
        title: 'Email',
        type: 'email',
        formatter: () => 0,
        parser: () => ''
      },
      {
        name: 'birthDate',
        title: 'Birth Date',
        type: 'date',
        formatter: () => 0,
        parser: () => ''
      },
      {
        name: 'basicSalary',
        title: 'Basic Salary',
        type: 'number',
        formatter: (value) => `Rp${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ','),
        parser: (value) => `${value}`.replace(/Rp\s?|(,*)/g, '')
      },
      {
        name: 'group',
        title: 'Group',
        type: 'select',
        options: EMPLOYEE_GROUP_MAPPER,
        formatter: () => 0,
        parser: () => ''
      },

      {
        name: 'description',
        title: 'Description',
        type: 'textarea',
        formatter: () => 0,
        parser: () => ''
      },
      {
        name: 'status',
        title: 'Status',
        type: 'select',
        options: ['Active', 'Non Active'],
        formatter: () => 0,
        parser: () => ''
      }
    ];

    this.autoTips = {
      default: { required: 'Input field must not be empty', email: 'The input is not valid E-mail!' }
    };
  }

  async onSubmitForm() {
    if (!this.form.valid) {
      throw new Error('Form is invalid');
    }

    const { error } = await this.employeeService.upsert({
      ...this.form.value,
      status: this.form.value.status === 'Active' ? 1 : 0
    });

    if (error) {
      this.message.error('Something is Error!', { nzDuration: 1500 });
      throw new Error(error.message || 'Something is Error!');
    }

    this.message.success('Employee created!', { nzDuration: 3000 });
    await this.router.navigateByUrl(`/employee`);
  }
}
