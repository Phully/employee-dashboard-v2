export type DataFormType = {
  name: string;
  title: string;
  type: string;
  options?: string[];
  disableDate?: (current: Date) => boolean;
  formatter: (value?: number | string) => number | string;
  parser: (value?: number | string) => string;
};
