import { bootstrapApplication } from '@angular/platform-browser';

import { MainComponent } from './app/main/main.component';

import { mainConfig } from './app/main/main.config';

bootstrapApplication(MainComponent, mainConfig).catch((err) => console.error(err));
